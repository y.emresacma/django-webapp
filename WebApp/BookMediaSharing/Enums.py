from django_enumfield import enum


class UserState(enum.Enum):
    NOT_VERIFIED = 0
    VERIFIED = 1


class FriendshipStatus(enum.Enum):
    NO_FRIEND = 0
    FRIEND = 1
    CLOSE_FRIEND = 2


class AccessState(enum.Enum):
    CLOSED = 0
    EVERYONE = 1
    FRIENDS = 2
    CLOSE_FRIENDS = 3


class Accessibility(enum.Enum):
    VIEW = 0
    DETAIL = 1
    BORROW = 2
    COMMENT = 3
    SEARCH = 4


# corresponds to type_ variable
class ItemType(enum.Enum):
    DVD = 0
    CD = 1
    BOOK = 2


class WatchMethod(enum.Enum):
    WATCH_COMMENT = 0
    WATCH_BORROW = 1


class Location(enum.Enum):
    ROOM = 0
    BOARD = 1
    SHELF = 2