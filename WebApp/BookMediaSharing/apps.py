from django.apps import AppConfig


class BookmediasharingConfig(AppConfig):
    name = 'BookMediaSharing'
