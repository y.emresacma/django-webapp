from django.contrib import admin
from .models import MyUser, Item, Friendship, FriendshipRequest, NotificationList, UserWatchList, BorrowRequest, Comment, Rate, ItemWatchList, Borrow

# Register your models here.
admin.site.register(MyUser)
admin.site.register(Item)
admin.site.register(Friendship)
admin.site.register(FriendshipRequest)
admin.site.register(NotificationList)
admin.site.register(UserWatchList)
admin.site.register(BorrowRequest)
admin.site.register(Comment)
admin.site.register(Rate)
admin.site.register(ItemWatchList)
admin.site.register(Borrow)
