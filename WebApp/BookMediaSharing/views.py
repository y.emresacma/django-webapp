from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from .forms import SignUpForm, FriendForm, AddItemForm, SetFriendForm, \
    WatchUserForm, ListItemsForm, RateForm, CommentForm, GetRatingForm, \
    GetCommentForm, GetViewInfoForm, GetDetailInfoForm, SendBorrowReqForm, \
    ChangeStatesForm, GiveItemForm, SearchItemForm, WatchItemForm, AnnounceForm, \
    ReturnItemForm, DeleteItemForm
from django.contrib.auth.decorators import login_required
from .models import MyUser, Item, Friendship, FriendshipRequest, NotificationList, UserWatchList, BorrowRequest, Comment, Rate, ItemWatchList, Borrow
from .Enums import FriendshipStatus, WatchMethod
from .Utility import retrieve_book_data, is_accessible
import datetime
# Create your views here.


def home(request):
    count = User.objects.count()
    return render(request, 'home.html', {
        'count' : count
    })


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.profile.name_surname = form.cleaned_data.get('name_surname')
            user.profile.email = form.cleaned_data.get('email')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


@login_required()
def profile_page(request):
    user = request.user
    friendrequests = FriendshipRequest.objects.filter(container = user.id)
    friendlist = Friendship.objects.filter(container = user.id)
    watchlist = UserWatchList.objects.filter(container = user.id)
    notification_list = NotificationList.objects.filter(container = user.id).order_by('-date')[:5]
    return render(request, 'profile_page.html', {'friendrequests' : friendrequests, 'friendlist' : friendlist,
                                                 'watchlist' : watchlist, 'notificationList': notification_list})


@login_required()
def friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            container = form.cleaned_data['container']
            email = request.user.email
            friendshiprequest = FriendshipRequest.create(container,email)
            friendshiprequest.save()


            return redirect('profile_page')
    else:
        form = FriendForm()
    return render(request, 'add_friend.html', {'form': form})

@login_required()
def set_friend(request):
    if request.method == 'POST':
        form = SetFriendForm(request.POST)
        if form.is_valid():
            friendship_status = form.cleaned_data['friendship_status']
            if(friendship_status == FriendshipStatus.NO_FRIEND):
                email = form.cleaned_data['email']
                FriendshipRequest.objects.filter(email = email).delete() # reject friendship request
                return redirect('profile_page')
            else:
                email = form.cleaned_data['email']
                FriendshipRequest.objects.filter(email=email).delete() # delete from friendship requestlist
                container = MyUser.objects.get(email = request.user.email)
                friendship = Friendship.create(container,email,friendship_status)
                friendship.save()

                container = MyUser.objects.get(email = email)
                email = request.user.email
                friendship = Friendship.create(container, email, friendship_status)
                friendship.save()
                return redirect('profile_page')

    else:
        form = SetFriendForm()
    return render(request, 'set_friend.html', {'form': form})


@login_required()
def watch(request):
    if request.method == 'POST':
        form = WatchUserForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            container = MyUser.objects.get(email=request.user.email)
            query = Friendship.objects.filter(container = container, email = email)

            if query.count() == 0:
                pass
            else:
                watch = UserWatchList.create(container, email)
                watch.save()


            return redirect('profile_page')
    else:
        form = WatchUserForm()

    return render(request, 'watch_user.html', {'form': form})


@login_required()
def add_item(request):
    if request.method == "POST":
        form = AddItemForm(request.POST)
        if form.is_valid():
            #item_instance = form.save(commit=False)
            uniqid = form.cleaned_data['uniqid']
            type = form.cleaned_data['type']
            location = form.cleaned_data['location']
            view_access_state = form.cleaned_data['view_access_state']
            detail_access_state = form.cleaned_data['detail_access_state']
            borrow_access_state = form.cleaned_data['borrow_access_state']
            comment_access_state = form.cleaned_data['comment_access_state']
            search_access_state = form.cleaned_data['search_access_state']

            user = MyUser.objects.get(email=request.user.email)
            watchList = UserWatchList.objects.filter(email=request.user.email)

            for watcher in watchList:
                friendship = Friendship.objects.get(container=user, email=watcher.container.email)
                friendship_status = FriendshipStatus.NO_FRIEND
                can_send_notf = False


                friendship_status = friendship.friendship_status

                if is_accessible(friendship_status, detail_access_state) or is_accessible(friendship_status, borrow_access_state):
                    can_send_notf = True


                # TODO cannot send notification
                watcher_user = MyUser.objects.get(email=watcher.container.email)
                if can_send_notf:
                    notf = NotificationList.create(watcher_user, str(user.email) + "'s has new item")
                    notf.save()
            try:
                title, year, artist, genre = retrieve_book_data(uniqid)
                item_instance = Item.create(user, type, location, title, year, artist, genre, uniqid, view_access_state,
                                            detail_access_state, borrow_access_state, comment_access_state, search_access_state)
                item_instance.save()
            except Exception:
                title = form.cleaned_data['title']
                year = form.cleaned_data['year']
                artist = form.cleaned_data['artist']
                genre = form.cleaned_data['genre']
                item_instance = Item.create(user, type, location, title, year, artist, genre, "0", view_access_state,
                                            detail_access_state, borrow_access_state, comment_access_state, search_access_state)
                item_instance.save()


            return redirect('profile_page')
    else:
        form = AddItemForm()
    return render(request, 'add_item.html', {'form': form})


@login_required()
def list_items(request):
    if request.method == "POST":
        form = ListItemsForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            user = MyUser.objects.get(email=email)
            items = Item.objects.filter(owner = user)
            friendship = Friendship.objects.filter(container=user, email=request.user.email)

            friendship_status = FriendshipStatus.NO_FRIEND
            if friendship.count() != 0:
                friendship_status = friendship.friendship_status

            result = list()
            for item in items:
                if is_accessible(friendship_status, item.view_access_state):
                    result.append(item)

            if form.is_valid():
                return render(request, 'list_items.html', {'form': form, 'items': result})
    else:
        form = ListItemsForm(request.POST)

    return render(request, 'list_items.html', {'form': form})

@login_required()
def items(request):
    user = request.user
    user = MyUser.objects.get(id=user.id)
    itemList = Item.objects.filter(owner = user)

    qSetList = list()
    for item in itemList:
        borrowedRequest = BorrowRequest.objects.filter(container = item)
        qSetList.append((item, borrowedRequest))

    borrowed_items = Borrow.objects.filter(email=request.user.email)
    borrowedItemsList = list()
    for borrow in borrowed_items:
        borrowedItemsList.append(borrow.container)


    return render(request, 'items.html',{'borrowed_items': borrowedItemsList, 'itemList': itemList, 'borrowReqList': qSetList})


#TODO check rate status, Users cannot rate all items
@login_required()
def rate(request):
    if request.method == "POST":
        form = RateForm(request.POST)
        if form.is_valid():
            container = form.cleaned_data['container']
            email = request.user.email
            rate = form.cleaned_data['rate']

            #check friendship status below TODO no checking here, might come NOFRIEND enum
            owner = container.owner
            query1 = Friendship.objects.filter(container=owner, email=email)

            tempUser = MyUser.objects.get(email=email)
            query2 = Friendship.objects.filter(container=tempUser, email=owner.email)

            if query1.count() == 0 and query2.count() == 0:
                pass
            else:
                rate_object = Rate.create(container, email, rate)
                rate_object.save()

                return redirect('items')
    else:
        form = RateForm(request.POST)

    return render(request, 'rate.html', {'form': form})


#TODO check comment status, Users cannot comment all items
@login_required()
def comment(request):
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            container = form.cleaned_data['container']
            email = request.user.email
            comment = form.cleaned_data['comment']

            owner = container.owner
            query1 = Friendship.objects.filter(container=owner, email=email)

            if query1.count() == 0:
                pass
            else:
                friendship_status = query1[0].friendship_status
                if is_accessible(friendship_status, container.comment_access_state):
                    comment_object = Comment.create(container, email, comment)
                    comment_object.save()

                return redirect('items')
    else:
        form = CommentForm(request.POST)

    return render(request, 'comment.html', {'form': form})


@login_required()
def get_rating(request):
    if request.method == "POST":
        form = GetRatingForm(request.POST)
        if form.is_valid():
            container = form.cleaned_data['container']
            rate_list = Rate.objects.filter(container = container.id)

            sum = 0.0
            total = 0
            for rate in rate_list:
                sum += rate.rate
                total += 1

            avg = sum / total

            return render(request, 'rating_result.html', {'rating': avg})
    else:
        form = GetRatingForm(request.POST)

    return render(request, 'get_rating.html', {'form': form})

#TODO check comment status
@login_required()
def get_comments(request):
    if request.method == "POST":
        form = GetCommentForm(request.POST)
        if form.is_valid():
            container = form.cleaned_data['container']

            owner = container.owner
            query1 = Friendship.objects.filter(container=owner, email=request.user.email)
            if query1.count() == 0:
                pass
            else:
                friendship_status = query1[0].friendship_status
                if is_accessible(friendship_status, container.comment_access_state):
                    comment_list = Comment.objects.filter(container = container.id)
                    return render(request, 'comment_results.html', {'comments': comment_list})
    else:
        form = GetCommentForm(request.POST)
    return render(request, 'get_comments.html', {'form': form})


@login_required()
def get_view_info(request):
    if request.method == "POST":
        form = GetViewInfoForm(request.POST)
        if form.is_valid():
            container = form.cleaned_data['container']
            view_info = Item.get_view_info(container.id)

            friendship_status = FriendshipStatus.NO_FRIEND
            owner = MyUser.objects.get(email=request.user.email)
            try:
                query1 = Friendship.objects.get(container=owner, email=container.owner.email)
                friendship_status = query1.friendship_status
            except:
                try:
                    query2 = Friendship.objects.get(container=container.owner, email=owner.email)
                    friendship_status = query2.friendship_status
                except:
                    pass
            if is_accessible(friendship_status, container.view_access_state):
                container.save()
                return render(request, 'view_info_results.html', {'view_info': view_info})
    else:
        form = GetViewInfoForm(request.POST)
    return render(request, 'get_view_info.html', {'form': form})


@login_required()
def get_detail_info(request):
    if request.method == "POST":
        form = GetDetailInfoForm(request.POST)
        if form.is_valid():
            container = form.cleaned_data['container']
            email = request.user.email
            detail_info = Item.get_detail_info(container.id, email)

            friendship_status = FriendshipStatus.NO_FRIEND
            owner = MyUser.objects.get(email=request.user.email)
            try:
                query1 = Friendship.objects.get(container=owner, email=container.owner.email)
                friendship_status = query1.friendship_status
            except:
                try:
                    query2 = Friendship.objects.get(container=container.owner, email=owner.email)
                    friendship_status = query2.friendship_status
                except:
                    pass

            if is_accessible(friendship_status, container.detail_access_state):
                container.save()
                return render(request, 'view_info_results.html', {'view_info': detail_info})
    else:
        form = GetDetailInfoForm(request.POST)
    return render(request, 'get_detail_info.html', {'form': form})



#TODO check borrow status, # try catch for unique constrain, cannot send borrow req twice
@login_required()
def send_borrow_req(request):
    if request.method == "POST":
        form = SendBorrowReqForm(request.POST)
        if form.is_valid():
            container = form.cleaned_data['container']
            email = request.user.email
            borrow_req = BorrowRequest.create(container, email)
            borrow_req.save()
            redirect('items')
    else:
        form = SendBorrowReqForm(request.POST)
    return render(request, 'send_borrow_req.html', {'form': form})


@login_required()
def change_states(request):
    if request.method == "POST":
        form = ChangeStatesForm(request.POST)
        if form.is_valid():
            item = form.cleaned_data['items']

            if item.owner.email != request.user.email:
                return render(request, 'error.html',  # TODO error message does not appear
                              {'message': 'You can only change states of your own items'})

            location = form.cleaned_data['location']
            view_access_state = form.cleaned_data['view_access_state']
            detail_access_state = form.cleaned_data['detail_access_state']
            borrow_access_state = form.cleaned_data['borrow_access_state']
            comment_access_state = form.cleaned_data['comment_access_state']
            search_access_state = form.cleaned_data['search_access_state']

            watch_list = ItemWatchList.objects.filter(container=item)

            if (item.comment_access_state != comment_access_state):
                for watcher in watch_list:
                    if watcher.watch_method == WatchMethod.WATCH_COMMENT:
                        watcher_user = MyUser.objects.get(email=watcher.email)
                        notf = NotificationList.create(watcher_user,
                                                       str(request.user.email) + " changed comment status")
                        notf.save()

            if (item.borrow_access_state != borrow_access_state):
                for watcher in watch_list:
                    if watcher.watch_method == WatchMethod.WATCH_BORROW:
                        watcher_user = MyUser.objects.get(email=watcher.email)
                        notf = NotificationList.create(watcher_user,
                                                       str(request.user.email) + " changed borrow status")
                        notf.save()

            item.location = location
            item.view_access_state = view_access_state
            item.detail_access_state = detail_access_state
            item.borrow_access_state = borrow_access_state
            item.comment_access_state = comment_access_state
            item.search_access_state = search_access_state
            item.save()

            redirect('items')
    else:
        form = ChangeStatesForm(request.POST)
    return render(request, 'change_states.html', {'form': form})


@login_required()
def give_item(request):
    if request.method == "POST":
        form = GiveItemForm(request.POST)
        if form.is_valid():
            item = form.cleaned_data['container']
            if item.owner.email != request.user.email:
                return render(request, 'error.html',
                              {'message': 'You can only give your own items'})

            borrow_reqs = BorrowRequest.objects.filter(container = item.id)
            borrow_req = borrow_reqs[0]
            borrow_requests = BorrowRequest.objects.filter(container=item.id, email=borrow_req.email)
            for borrow_req in borrow_requests:
                borrow_req.delete()

            access_state = item.borrow_access_state

            user = MyUser.objects.get(email=request.user.email)
            friendship_status = FriendshipStatus.NO_FRIEND
            try:
                friendship = Friendship.objects.get(container=user, email=borrow_req.email)
                friendship_status = friendship.friendship_status
            except:
                pass

            if is_accessible(friendship_status, access_state):
                item.is_available = False
                d = datetime.timedelta(days=2, seconds=0)
                req = Borrow.create(container=item, email=borrow_req.email, time=d)
                req.save()
                item.save()
                return redirect('items')

    else:
        form = GiveItemForm(request.POST)
    return render(request, 'give_item.html', {'form': form})

@login_required()
def search_item(request):
    if request.method == "POST":
        form = SearchItemForm(request.POST)
        if form.is_valid():
            artist = form.cleaned_data['artist']
            title = form.cleaned_data['title']
            year1 = form.cleaned_data['year1']
            year2 = form.cleaned_data['year2']
            for_borrow = form.cleaned_data['for_borrow']

            items = Item.objects.all()
            result = list()


            if for_borrow:
                for item in items:
                    is_borrowable = False
                    try:
                        friendship = Friendship.objects.get(container=item.owner, email=request.user.email)
                        is_borrowable = is_accessible(friendship.friendship_status, item.borrow_access_state)
                    except:
                        pass

                    if is_borrowable:
                        if artist.lower() == item.artist.lower() or title.lower() == item.year.lower():
                            result.append(item)
                        else:
                            try:
                                if item.year >=year1 and item.year <= year2:
                                    result.append(item)
                            except:
                                try:
                                    if item.year == year1:
                                        result.append(item)
                                except:
                                    pass
            else:
                for item in items:
                    if artist.lower() == item.artist.lower() or title.lower() == item.year.lower():
                        result.append(item)
                    else:
                        try:
                            if item.year >=year1 and item.year <= year2:
                                result.append(item)
                        except:
                            try:
                                if item.year == year1:
                                    result.append(item)
                            except:
                                pass

            return render(request, 'search_result.html', {'items': result})

    else:
        form = SearchItemForm(request.POST)
    return render(request, 'search_item.html', {'form': form})

@login_required()
def watch_item(request):
    if request.method == 'POST':
        form = WatchItemForm(request.POST)
        if form.is_valid():
            container = form.cleaned_data['container']
            watch_method = form.cleaned_data['watch_method']

            if container.owner.email == request.user.email:
                return render(request, 'error.html',  # TODO error message does not appear
                              {'message': 'You cannot watch your own item'})

            can_watch = False
            friendship_status = FriendshipStatus.NO_FRIEND
            try:
                friendship = Friendship.objects.get(container=container.owner, email=request.user.email)
                friendship_status = friendship.friendship_status
            except:
                pass

            if watch_method == WatchMethod.WATCH_COMMENT:
                if is_accessible(friendship_status, container.detail_access_state):
                    can_watch = True
            elif watch_method == WatchMethod.WATCH_BORROW:
                if is_accessible(friendship_status, container.borrow_access_state):
                    can_watch = True

            if can_watch:
                watch = ItemWatchList.create(container, request.user.email, watch_method)
                watch.save()


            return redirect('items')
    else:
        form = WatchItemForm(request.POST)

    return render(request, 'watch_item.html', {'form': form})

@login_required()
def announce_item(request):
    if request.method == 'POST':
        form = AnnounceForm(request.POST)
        if form.is_valid():
            item = form.cleaned_data['item']

            if item.owner.email != request.user.email:
                return render(request, 'error.html',  # TODO error message does not appear
                              {'message': 'You can only announce your own item'})

            friends = Friendship.objects.filter(container=item.owner)

            for friend in friends:
                user = MyUser.objects.get(email=friend.email)
                notf = NotificationList.create(user, "Come and get my item : title -> " + str(item.title))
                notf.save()
            return redirect('items')
    else:
        form = AnnounceForm(request.POST)

    return render(request, 'announce_item.html', {'form': form})

@login_required()
def return_item(request):
    if request.method == 'POST':
        form = ReturnItemForm(request.POST)
        if form.is_valid():
            item = form.cleaned_data['container']

            borrows = Borrow.objects.filter(email=request.user.email)

            for borrow in borrows:
                if borrow.container.id == item.id:
                    borrow.delete()

                    item.is_available = True
                    return render(request, 'items.html')


    else:
        form = ReturnItemForm(request.POST)

    return render(request, 'return_item.html', {'form': form})

@login_required()
def delete_item(request):
    if request.method == 'POST':
        form = DeleteItemForm(request.POST)
        if form.is_valid():
            item = form.cleaned_data['item']

            if item.owner.email == request.user.email:
                item.delete()
                item.save()

    else:
        form = DeleteItemForm(request.POST)

    return render(request, 'delete_item.html', {'form': form})