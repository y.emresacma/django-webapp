from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import MyUser, Item, Friendship, FriendshipRequest, NotificationList, UserWatchList, BorrowRequest, Comment, Rate, ItemWatchList, Borrow
from django.core.validators import MaxValueValidator, MinValueValidator

class SignUpForm(UserCreationForm):
    name_surname = forms.CharField(max_length=30)
    email = forms.EmailField(max_length=30)
    class Meta:
        model = User
        fields = ('username', 'name_surname', 'email', 'password1', 'password2',)


class FriendForm(forms.ModelForm):
    class Meta:
        model = FriendshipRequest
        fields = ('container',)


class SetFriendForm(forms.ModelForm):
    class Meta:
        model = Friendship
        fields = ('email', 'friendship_status',)


class WatchUserForm(forms.ModelForm):
    class Meta:
        model = UserWatchList
        fields = ('email',)


class AddItemForm(forms.ModelForm):
    class Meta:
        model = Item
        uniqid = forms.CharField(required=False)
        fields = ('title','artist', 'genre', 'year', 'type', 'location','view_access_state','detail_access_state',
                  'borrow_access_state', 'comment_access_state', 'search_access_state','uniqid', )


class ListItemsForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('email',)


class RateForm(forms.ModelForm):
    class Meta:
        model = Rate
        fields = ('container','rate',)


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('container', "comment",)


class GetRatingForm(forms.ModelForm):
    class Meta:
        model = Rate
        fields = ('container',)


class GetCommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('container',)


class GetViewInfoForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('container',)


class GetDetailInfoForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('container',)


class SendBorrowReqForm(forms.ModelForm):
    class Meta:
        model = BorrowRequest
        fields = ('container',)


class ChangeStatesForm(forms.ModelForm):
    items = forms.ModelChoiceField(queryset=Item.objects.all())

    class Meta:
        model = Item
        fields = ('location', 'view_access_state', 'detail_access_state',
                  'borrow_access_state', 'comment_access_state', 'search_access_state',)


class GiveItemForm(forms.ModelForm):
    class Meta:
        model = Borrow
        fields = ('container',)

class SearchItemForm(forms.Form):
    artist = forms.CharField(max_length=30)
    title = forms.CharField(max_length=30)
    year1 = forms.IntegerField()
    year2 = forms.IntegerField()
    for_borrow = forms.BooleanField(required=False)


class WatchItemForm(forms.ModelForm):
    class Meta:
        model = ItemWatchList
        fields= ('container', 'watch_method',)


class AnnounceForm(forms.Form):
    item = forms.ModelChoiceField(queryset=Item.objects.all())


class ReturnItemForm(forms.ModelForm):
    class Meta:
        model = Borrow
        fields = ('container', )


class DeleteItemForm(forms.Form):
    item = forms.ModelChoiceField(queryset=Item.objects.all())

