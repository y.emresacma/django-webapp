import random
import json
import urllib.request
from .Enums import Accessibility, AccessState

def generateVerificationCode():
    return str(random.randrange(100000, 999999))


def generateTemporaryPassword():
    return str(random.randrange(100000000, 999999999))


def retrieve_book_data(uniqId):
    base_api_link = "https://www.googleapis.com/books/v1/volumes?q=isbn:"
    uniqId = str(uniqId)
    with urllib.request.urlopen(base_api_link + uniqId) as f:
        text = f.read()

    decoded_text = text.decode("utf-8")
    obj = json.loads(decoded_text) # deserializes decoded_text to a Python object

    try:
        volume_info = obj["items"][0]
    except KeyError:
        raise Exception("Invalid id")

    try:
        title = volume_info["volumeInfo"]["title"]
    except KeyError:
        title = None

    try:
        year = volume_info["volumeInfo"]["publishedDate"]
        if year.find('-'):
            year = year[:4] + year[:2] + year[5:7]  # example '2004-08' -> 2004-2008
    except KeyError:
        year = None

    try:
        author = volume_info["volumeInfo"]["authors"][0]
    except KeyError:
        author = None

    try:
        categories = volume_info["volumeInfo"]["categories"][0]
    except KeyError:
        categories = None

    return title, year, author, categories


def is_accessible(friendship_status, access):
    if access == AccessState.EVERYONE:
        return True

    if access != AccessState.CLOSED and (friendship_status.value + 1 >= access.value):
        return True

    return False
