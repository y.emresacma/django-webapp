from django.urls import path,include
from django.conf.urls import url
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^add_friend/$', views.friend, name='add_friend'),
    url(r'^watch_user/$', views.watch, name='watch_user'),
    url(r'^set_friend/$', views.set_friend, name='set_friend'),
    url(r'^add_item/$', views.add_item, name='add_item'),
    url(r'^list_item/$', views.list_items, name='list_item'),
    url(r'^items/$', views.items, name='items'),
    url(r'^rate/$', views.rate, name='rate'),
    url(r'^comment/$', views.comment, name='comment'),
    url(r'^get_rating/$', views.get_rating, name='get_rating'),
    url(r'^get_comments/$', views.get_comments, name='get_comments'),
    url(r'^profile_page/$', views.profile_page, name='profile_page'),
    url(r'^get_view_info/$', views.get_view_info, name='get_view_info'),
    url(r'^get_detail_info/$', views.get_detail_info, name='get_detail_info'),
    url(r'^send_borrow_req/$', views.send_borrow_req, name='send_borrow_req'),
    url(r'^change_states/$', views.change_states, name='change_states'),
    url(r'^give_item/$', views.give_item, name='give_item'),
    url(r'^search_item/$', views.search_item, name='search_item'),
    url(r'^watch_item/$', views.watch_item, name='watch_item'),
    url(r'^announce_item/$', views.announce_item, name='announce_item'),
    url(r'^return_item/$', views.return_item, name='return_item'),
    url(r'^delete_item/$', views.delete_item, name='delete_item'),
    path('accounts/', include('django.contrib.auth.urls')),
]