from django.db import models
from .Utility import generateVerificationCode, generateTemporaryPassword, retrieve_book_data
from .Enums import UserState, FriendshipStatus, WatchMethod, AccessState, ItemType, Location
from django_enumfield import enum
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
import datetime

# Create your models here.
class MyUser(models.Model):
    #user_id = models.AutoField(primary_key=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE,related_name='profile')
    email = models.EmailField(max_length=30)  # TODO
    name_surname = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    user_state = enum.EnumField(UserState, default=UserState.NOT_VERIFIED)
    verification_code = models.CharField(max_length=30, default=generateVerificationCode)


class Friendship(models.Model):
    container = models.ForeignKey(MyUser, db_index=True, on_delete=models.CASCADE)
    email = models.EmailField(max_length=30)
    friendship_status = enum.EnumField(FriendshipStatus, default=FriendshipStatus.NO_FRIEND)

    class Meta:
        unique_together = (('container', 'email'),)

    @classmethod
    def create(cls, container, email,friendship_status):
        friendship = cls(container=container, email=email, friendship_status = friendship_status)
        # do something with the book
        return friendship

class FriendshipRequest(models.Model):
    container = models.ForeignKey(User, db_index=True, on_delete=models.CASCADE)
    email = models.EmailField(max_length=30)

    class Meta:
        unique_together = (('container', 'email'),)

    @classmethod
    def create(cls, container, email):
        friendshiprequest = cls(container=container, email = email)
        # do something with the book
        return friendshiprequest



class NotificationList(models.Model):
    container = models.ForeignKey(MyUser, db_index=True, on_delete=models.CASCADE)
    notification = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    @classmethod
    def create(cls, container, notification):
        notf = cls(container=container, notification=notification)
        # do something with the book
        return notf


class UserWatchList(models.Model):
    container = models.ForeignKey(MyUser, db_index=True, on_delete=models.CASCADE)
    email = models.EmailField(max_length=30)

    class Meta:
        unique_together = (('container', 'email'),)

    @classmethod
    def create(cls, container, email):
        watch = cls(container=container, email=email)
        # do something with the book
        return watch


#---------------------------------USER ENDS---------------------------------#


class Item(models.Model):
    # item_id = models.AutoField(primary_key=True)
    owner = models.ForeignKey(MyUser, on_delete=models.CASCADE)
    title = models.CharField(max_length=30, null=False,blank=True)
    artist = models.CharField(max_length=30, null=False,blank=True)
    genre = models.CharField(max_length=30, null=False,blank=True)
    year = models.CharField(max_length=10, null=False,blank=True)
    type = enum.EnumField(ItemType)
    location = enum.EnumField(Location)
    uniqid = models.CharField(max_length=15,  null=False,blank=True, default=None)

    view_access_state = enum.EnumField(AccessState, default=AccessState.CLOSED)
    detail_access_state = enum.EnumField(AccessState, default=AccessState.CLOSED)
    borrow_access_state = enum.EnumField(AccessState, default=AccessState.CLOSED)
    comment_access_state = enum.EnumField(AccessState, default=AccessState.CLOSED)
    search_access_state = enum.EnumField(AccessState, default=AccessState.CLOSED)

    is_available = models.BooleanField(default=True)

    @classmethod
    def create_fromid(cls, owner, type, location, uniqid):
        title, year, artist, genre = retrieve_book_data(uniqid)
        book = cls(owner=owner, title=title, year=year, artist=artist, genre=genre, type=type, location=location, uniqid=uniqid)
        # do something with the book
        return book


    @classmethod
    def create(cls, owner, type, location, title, year, artist, genre, uniqid, view_access_state,
                                            detail_access_state, borrow_access_state, comment_access_state, search_access_state):
        book = cls(owner=owner, title=title, year=year, artist=artist, genre=genre, type=type, location=location, uniqid=uniqid,
                   view_access_state=view_access_state, detail_access_state=detail_access_state, borrow_access_state=borrow_access_state,
                   comment_access_state=comment_access_state, search_access_state=search_access_state)
        # do something with the book
        return book

    @staticmethod
    def get_view_info(id_):
        item = Item.objects.filter(id=int(id_))[0]
        return [item.owner.email, str(item.type), item.title]

    @staticmethod
    def get_detail_info(id_, email):
        item = Item.objects.filter(id=int(id_))[0]
        return_list = [item.owner.email, str(item.type), item.title, item.artist, item.genre, item.year]

        if item.owner.email == email:
            return_list += [item.location]

        return return_list


class BorrowRequest(models.Model):
    container = models.ForeignKey(Item, db_index=True, on_delete=models.CASCADE)
    email = models.EmailField(max_length=30)

    class Meta:
        unique_together = (('container', 'email'),)

    @classmethod
    def create(cls, container, email):  # TODO check container.owner.email not email
        try:
            book = cls(container=container, email=email)
            return book
        except:
            return None


class Comment(models.Model):
    container = models.ForeignKey(Item, db_index=True, on_delete=models.CASCADE)
    email = models.EmailField(max_length=30)
    comment = models.TextField()

    @classmethod
    def create(cls, container, email, comment):
        comment = cls(container = container, email = email, comment = comment)
        # do something with the book
        return comment


class Rate(models.Model):
    container = models.ForeignKey(Item, db_index=True, on_delete=models.CASCADE)
    email = models.EmailField(max_length=30)
    rate = models.IntegerField(
        default=1,
        validators=[MaxValueValidator(5), MinValueValidator(1)]
     )

    class Meta:
        unique_together = (('container', 'email'),)

    @classmethod
    def create(cls, container, email, rate):
        rate = cls(container = container, email = email, rate = rate)
        # do something with the book
        return rate


class ItemWatchList(models.Model):
    container = models.ForeignKey(Item, db_index=True, on_delete=models.CASCADE)
    email = models.EmailField(max_length=30)
    watch_method = enum.EnumField(WatchMethod)

    class Meta:
        unique_together = (('container', 'email'),)

    @classmethod
    def create(cls, container, email, watch_method):
        watch = cls(container=container, email=email,watch_method = watch_method)
        # do something with the book
        return watch


class Borrow(models.Model):
    container = models.ForeignKey(Item, db_index=True, on_delete=models.CASCADE)
    email = models.EmailField(max_length=30)
    time = models.DurationField()

    @classmethod
    def create(cls, container, email, time):
        borrow = cls(container = container, email = email, time = time)
        # do something with the book
        return borrow

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        MyUser.objects.create(user=instance)
    instance.profile.save()


